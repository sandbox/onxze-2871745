<?php

/**
 * Implements hook_views_data().
 */
function vjq_views_data() {
    $data = [];
    // Base data.
    $data['vjq']['table']['group'] = t('VJQ query');
    $data['vjq']['table']['base'] = [
        'title' => t('VJQ query'),
        'help' => t('Query for external REST API.'),
        'query_id' => 'vjq_query',
    ];
    // Fields.
    $data['vjq']['json_value'] = [
        'title' => t('JSON value'),
        'help' => t('JSON value field.'),
        'field' => [
            'id' => 'vjq_field',
        ],
        'filter' => [
          'id' => 'vjq_filter',
        ],
        'sort' => [
          'id' => 'vjq_sort',
        ],
    ];
    return $data;
}
