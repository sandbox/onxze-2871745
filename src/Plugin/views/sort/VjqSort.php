<?php

namespace Drupal\vjq\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Sorts Json results.
 * 
 * From Views Json Query. Usable?
 *
 * @ViewsSort("vjq_sort")
 */
class VjqSort extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['json_key'] = array(
      'default' => '',
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['json_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Json key'),
      '#default_value' => $this->options['json_key'],
      '#description' => $this->t('The key of the field you want to order by.<br />Example: Using the following JSON and given you need to sort by "amount", then you would set this field to "amount".<br />!example', array(
        '!example' => '$this->getJsonExample()',
      )),
      '#required' => TRUE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->query->addOrderBy('', $this->options['json_key'], $this->options['order']);
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->options['json_key'] . ' [' . parent::adminSummary() . ']';
  }

}
