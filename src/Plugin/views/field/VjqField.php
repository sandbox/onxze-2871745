<?php

namespace Drupal\vjq\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;

/**
 * Class JSON value
 *
 * @ViewsField("vjq_field")
 */
class VjqField extends FieldPluginBase
{

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['json_key'] = array(
      'default' => 'value',
    );

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['json_key'] = [
      '#type' => 'textfield',
      '#title' => t('Json key'),
      '#default_value' => $this->options['json_key'],
      '#description' => $this->t('Key to column in json data.'),
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->options['json_key']) {
      $this->table_alias = 'json';
      $this->field_alias = $this->query->addField(
        'json',
        $this->options['json_key'],
        $this->options['json_key']
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    return isset($values->{$this->options['json_key']}) ? $values->{$this->options['json_key']} : '';
  }

}
