<?php

namespace Drupal\vjq\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Filters Json results.
 *
 * @ViewsFilter("vjq_filter")
 */
class VjqFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['json_condition'] = array(
      'default' => '',
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['json_condition'] = array(
      '#type' => 'textfield',
      '#title' => t('Json condition'),
      '#default_value' => $this->options['json_condition'],
      '#description' => $this->t('Filter condition field.'),
      '#required' => TRUE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $summary = parent::adminSummary();

    if (!empty($this->options['json_condition'])) {
      $summary = $this->options['json_condition'] . ' ' . $summary;
    }
    if (!empty($this->options['exposed'])) {
      $summary .= '- ' . $this->t('exposed');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#size' => 30,
      '#default_value' => $this->value,
    ];
  }

  /**
   * Add this filter to the query.
   *
   * Due to the nature of fapi, the value and the operator have an unintended
   * level of indirection. You will find them in $this->operator
   * and $this->value respectively.
   */
  public function query() {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

  /**
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   */
  function operators() {
    $operators = array(
      '=' => array(
        'title' => $this->t('Is equal to'),
        'short' => $this->t('='),
        'method' => 'opEqual',
        'values' => 1,
      ),
      'contains' => array(
        'title' => $this->t('Contains'),
        'short' => $this->t('contains'),
        'method' => 'opContains',
        'values' => 1,
      ),
      '>' => array(
        'title' => $this->t('Greater than'),
        'short' => $this->t('greater than'),
        'method' => 'opGreater',
        'values' => 1,
      ),
      '<' => array(
        'title' => $this->t('Less than'),
        'short' => $this->t('less than'),
        'method' => 'opLess',
        'values' => 1,
      ),
      '>=' => array(
        'title' => $this->t('Greater or equal'),
        'short' => $this->t('greater or equal'),
        'method' => 'opGreaterEqual',
        'values' => 1,
      ),
      '<=' => array(
        'title' => $this->t('Less or equal'),
        'short' => $this->t('less or equal'),
        'method' => 'opLessEqual',
        'values' => 1,
      ),
      'start_time' => array(
        'title' => $this->t('Start datetime'),
        'short' => $this->t('start datetime'),
        'method' => 'opStartDatetime',
        'values' => 1,
      ),
      'end_time' => array(
        'title' => $this->t('End datetime'),
        'short' => $this->t('end datetime'),
        'method' => 'opEndDatetime',
        'values' => 1,
      ),
    );

    return $operators;
  }

  /**
   * Build strings from the operators() for 'select' options
   */
  public function operatorOptions($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  public function opEqual($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, '=');
  }

  protected function opContains($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, 'contains');
  }

  public function opGreater($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, '>');
  }

  public function opLess($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, '<');
  }

  public function opGreaterEqual($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, '>=');
  }

  public function opLessEqual($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, '<=');
  }
  public function opStartDatetime($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, 'start_time');
  }
  public function opEndDatetime($field) {
    if ($field == '.json_value') {
      $real_field = $this->options['json_condition'];
    }
    $this->query->addWhere($this->options['group'], $real_field, $this->value, 'end_time');
  }

}
