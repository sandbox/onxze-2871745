<?php
namespace Drupal\vjq\Plugin\views\query;

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Fitbit views query plugin which wraps calls to the Fitbit API in order to
 * expose the results to views.
 *
 * @ViewsQuery(
 *   id = "vjq_query",
 *   title = @Translation("VJQ"),
 *   help = @Translation("Query against REST API.")
 * )
 */
class VjqQuery extends QueryPluginBase
{
  /**
   * A simple array of order by clauses.
   */
  public $orderby = array();

  /**
   * Key used in sort.
   */
  public $sortKey;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['json_file'] = ['default' => ''];
    $options['row_apath'] = ['default' => ''];
    $config = \Drupal::config('vjq.settings');
    $options['use_auth'] = ['default' => $config->get('json_server.authorization')];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['json_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Json File'),
      '#default_value' => $this->options['json_file'],
      '#description' => t("The URL or path to the Json file."),
      '#maxlength' => 1024,
    );
    $form['row_apath'] = array(
      '#type' => 'textfield',
      '#title' => t('Row Apath'),
      '#default_value' => $this->options['row_apath'],
      '#description' => t("Apath to records.<br />Apath is just a simple array item find method. Ex:<br /><pre>array('data' => \n\tarray('records' => \n\t\tarray(\n\t\t\tarray('name' => 'yarco', 'sex' => 'male'),\n\t\t\tarray('name' => 'someone', 'sex' => 'male')\n\t\t)\n\t)\n)</pre><br />You want 'records', so Apath could be set to 'data/records'. <br />Notice: prefix '/' or postfix '/' will be trimed, so never mind you add it or not."),
    );
    $form['use_auth'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use authentication'),
      '#default_value' => $this->options['use_auth'],
      '#description' => t("Add autohorization key to header."),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = array()) {
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function build(ViewExecutable $view) {
    $this->view = $view;
    $view->initPager();
    $this->view->pager->query();
    $this->view->build_info['query'] = '';
    $this->view->build_info['count_query'] = '';
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    $results = $this->getJsonData();
    $code = isset($results['code']) ? $results['code'] : '999';
    if ($code == 999) {
      $view->result = [];
      drupal_set_message(t('Data retrieve failed. Contact site administrator.'), 'error');
      return;
    }
    unset($results['code']);
    $rows = [];
    // Check that we have wrapper array.
    if (!is_array(current($results))) {
      $results = [$results];
    }
    // If no main defined, use as it is.
    if (empty($this->options['row_apath'])) {
      $rows = $results;
    }
    else {
      if (isset($results[$this->options['row_apath']])) {
        $rows = empty($results[$this->options['row_apath']]) ? $results : $results[$this->options['row_apath']];
      }
    }
    // Filter data.
    $view_result = $this->doFilters($rows);
    // Apply order by, result passed as reference.
    $this->doSort($view_result);
    // Add pagers if needed.
    $view->result = $this->doPager($view, $view_result);
    $view->pager->postExecute($view->result);
    $view->pager->updatePageInfo();
    $view->total_rows = $view->pager->getTotalItems();
  }

  /**
   * Make http query to REST API.
   *
   * @return array
   *    Query result.
   */
  protected function getJsonData() {
    $client = \Drupal::httpClient();
    $config = \Drupal::config('vjq.settings');
    $request_url = $this->getJsonFileUri($config);
    $options = $this->setVjqHeader($config);

    try {
      $response = $client->get($request_url, $options);
      $code = $response->getStatusCode();
      $body = json_decode($response->getBody());
      $body_array = (array) $body;
      $body_array['code'] = $code;
      return $body_array;
    } // ToDo: Better exeption handling. Now we assume that error happens in Guzzle response.
    catch (RequestException $e) {
      watchdog_exception('Vjq', $e);
      if ($e->getResponse() != NULL) {
        $http_return = $e->getResponse()->getStatusCode();
      }
      else {
        \Drupal::logger('Vjq')->error('No response from API.');
        $http_return = 999;
      }
      return ['return code' => $http_return];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }
    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }

    $this->where[$group]['conditions'][] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * Filter row data.
   *
   * @param array $filters
   *    Filter data.
   * @param array $row
   *    Row data.
   *
   * @return type
   *
   */
  protected function filterData($filters, $row) {
//    array(1)
//      array(3)
//        'field_name' => string(3) "uid"
//        'value' => string(2) "61"
//        'operation' => string(1) "="

    // We have atlest one filter when are in here and because filters are
    // AND type, we can set initial value to true.
    $result = TRUE;
    foreach ($filters as $filter) {
      if (isset($row[$filter['field_name']])) {
        $result &= $this->filterOperations($filter['operation'], $row[$filter['field_name']], $filter['value']);
      }
    }
    return $result;
  }

  /**
   * Define operations for using in filter.
   *
   * @param string $operation
   *    Used operation.
   * @param mixed $left
   *    Left side parameter.
   * @param mixed $right
   *    Right side parameter.
   *
   * @return boolean
   *    True if match, else false
   */
  protected function filterOperations($operation, $left, $right) {
    if ($operation == 'start_time' || $operation == 'end_time') {
      $left = is_numeric($left) ? $left : strtotime($left);
      $right = is_numeric($right) ? $right : strtotime($right);
    }
    $table = array(
      '=' => create_function('$left,$right', 'return $left == $right;'),
      'not empty' => create_function('$left,$right', 'return !empty($left);'),
      '!=' => create_function('$left,$right', 'return $left !== $right;'),
      'contains' => create_function('$left, $right', 'return stripos($left, $right) !== false;'),
      '!contains' => create_function('$left, $right', 'return stripos($left, $right) === false;'),
      'shorterthan' => create_function('$left, $right', 'return strlen($left) < $right;'),
      'longerthan' => create_function('$left, $right', 'return strlen($left) > $right;'),
      '>' => create_function('$left, $right', 'return $left > $right;'),
      '>=' => create_function('$left, $right', 'return $left >= $right;'),
      '<' => create_function('$left, $right', 'return $left < $right;'),
      '<=' => create_function('$left, $right', 'return $left <= $right;'),
      'start_time' => create_function('$left, $right', 'return $left >= $right;'),
      'end_time' => create_function('$left, $right', 'return $left <= $right;'),
    );

    return call_user_func_array($table[$operation], array($left, $right));
  }

  /**
   * Return URL or file path to JSON data.
   *
   * If json server path is given in Query settings as full
   * url (http://server.domain.ext/path) it is used. But only path is given
   * server information read from settings.
   *
   * @param object $config
   *    VJQ module configuration.
   *
   * @return string
   *    Path to json resource.
   */
  public function getJsonFileUri($config) {
    if (strpos($this->options['json_file'], 'http') === 0) {
      return $this->options['json_file'];
    }

    $protocol = explode(':', $config->get('json_server.protocol'));
    $url =  $protocol[0] . '://';
    $url .= $config->get('json_server.url');
    if (!empty($config->get('json_server.port'))) {
      $url .= ':' . $config->get('json_server.port');
    }
    $path = ltrim($this->options['json_file'], '/');
    $url .= '/' . $path;
    return $url;
  }

  /**
   * Create http header options.
   *
   * @param object $config
   *    VJQ configuration items.
   *
   * @return array
   *    Header data.
   */
  protected function setVjqHeader($config) {
    $options = [];

    if (isset($this->options['use_auth'])) {
      $authPrefix = $config->get('json_server.authorization_prefix');

      $authFieldName = $config->get('json_server.authorization_field');
      $apiKey = '';
      if (!empty($authFieldName)) {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $apiKey = $user->get($authFieldName)->value;
      }
      $options['headers'] = ['Authorization' => $authPrefix . $apiKey];
    }

    return $options;
  }

  /**
   * Add an ORDER BY clause to the query.
   *
   * @param $table
   *   The table this field is part of. If a formula, enter NULL.
   *   If you want to orderby random use "rand" as table and nothing else.
   * @param $field
   *   The field or formula to sort on. If already a field, enter NULL
   *   and put in the alias.
   * @param $order
   *   Either ASC or DESC.
   * @param $alias
   *   The alias to add the field as. In SQL, all fields in the order by
   *   must also be in the SELECT portion. If an $alias isn't specified
   *   one will be generated for from the $field; however, if the
   *   $field is a formula, this alias will likely fail.
   * @param $params
   *   Any params that should be passed through to the addField.
   */
  public function addOrderBy($table, $field = NULL, $order = 'ASC', $alias = '', $params = array()) {
    $afield = empty($field) ? $alias : $field;
    $this->orderby[] = [
      'field' => $afield,
      'order' => $order,
    ];
  }

  /**
   * Function for usort used to sorts two results ascendantly.
   *
   * @param array $a
   *   First element to compare.
   * @param array $b
   *   Second element to compare.
   *
   * @return int
   *   Sort result.
   */
  private function sortAsc($a, $b) {
    $left = isset($a->{$this->sortKey}) ? $a->{$this->sortKey} :'';
    $right = isset($b->{$this->sortKey}) ? $b->{$this->sortKey} :'';
    return strcasecmp($left, $right);
  }

  /**
   * Function for usort used to sorts two results descendantly.
   *
   * @param array $a
   *   First element to compare.
   * @param array $b
   *   Second element to compare.
   *
   * @return int
   *   Sort result.
   */
  private function sortDesc($a, $b) {
    $left = isset($a->{$this->sortKey}) ? $a->{$this->sortKey} :'';
    $right = isset($b->{$this->sortKey}) ? $b->{$this->sortKey} :'';
    return -strcasecmp($left, $right);
  }

  /**
   * Sort Numbers Ascending.
   *
   * @param array $a
   *   First element to compare.
   * @param array $b
   *   Second element to compare.
   *
   * @return int
   *   Sort result.
   */
  function sortAscNum($a, $b) {
    $a_value = isset($a->{$this->sortKey}) ? $a->{$this->sortKey} : 0;
    $b_value = isset($b->{$this->sortKey}) ? $b->{$this->sortKey} : 0;

    return $a_value > $b_value;
  }

  /**
   * Sort Numbers Descending.
   *
   * @param array $a
   *   First element to compare.
   * @param array $b
   *   Second element to compare.
   *
   * @return int
   *   Sort result.
   */
  function sortDescNum($a, $b) {
    $a_value = isset($a->{$this->sortKey}) ? $a->{$this->sortKey} : 0;
    $b_value = isset($b->{$this->sortKey}) ? $b->{$this->sortKey} : 0;

    return $a_value < $b_value;
  }

  /**
   * {@inheritdoc}
   */
  protected function doSort(&$results) {
    // Apply order by.
    if (!empty($this->orderby)) {
      foreach (array_reverse($this->orderby) as $orderby) {
        if (empty($orderby['field'])) {
          return;
        }
        $this->sortKey = $orderby['field'];
        $curr = current($results);
        $is_number = is_numeric($curr->{$this->sortKey});

        switch (strtoupper($orderby['order'])) {
          case 'ASC':
            if ($is_number) {
              usort($results, array($this, 'sortAscNum'));
            }
            else {
              usort($results, array($this, 'sortAsc'));
            }
            break;

          default:
            if ($is_number) {
              usort($results, array($this, 'sortDescNum'));
            }
            else {
              usort($results, array($this, 'sortDesc'));
            }
        }
      }
    }
  }

  /**
   * Add pager to view if so set.
   *
   * @param ViewExecutable $view
   *    Current view.
   * @param array $view_result
   *    Current resultset
   *
   * @return array
   *    Paged resultset.
   */
  protected function doPager(ViewExecutable $view, $view_result) {
    if ($view->getPager()->getPluginId() == 'none') {
      return $view_result;
    }

    $view->get_total_rows = TRUE;

    $pager = $this->view->pager;
    // Do we have exposed item count.
    if (isset($pager->view->exposed_data['items_per_page'])) {
      $pager->options['items_per_page'] =
        is_numeric($pager->view->exposed_data['items_per_page']) ?
          $pager->view->exposed_data['items_per_page'] : 0;
    }

    $original_offset = empty($pager->options['offset']) ? 0 : $pager->options['offset'];
    $offset = empty($this->offset) ? 0 : $this->offset;
    $limit = empty($pager->options['items_per_page']) ? 0 : $pager->options['items_per_page'];

    if ($pager->useCountQuery() || !empty($view->get_total_rows)) {
      $pager->total_items = count($view_result) - $original_offset;
      $pager->updatePageInfo();
    }
    $this->view->total_rows = count($view_result) - $original_offset;

    // Apply offset and limit.
    if ($limit) {
      $result_rows = array_slice($view_result, $offset, $limit);
    }
    else {
      $result_rows = array_slice($view_result, $offset);
    }

    return $result_rows;
  }

  protected function doFilters($rows) {
    $filters = [];
    if (isset($this->where)) {
      foreach ($this->where as $where_group => $where) {
        foreach ($where['conditions'] as $condition) {
          // For only one value given.
          $filterValue = current($condition['value']);
          if (!empty($filterValue)) {
            $filters[] = array(
              'field_name' => ltrim($condition['field'], '.'),
              'value' => $filterValue,
              'operation' => $condition['operator'],
            );
          }
        }
      }
    }

    $index = 0;
    $view_result = [];
    foreach ($rows as $data) {
      $row = (array)$data;
      $row['index'] = $index++;
      if (empty($filters) || $this->filterData($filters, $row)) {
        $view_result[] = new ResultRow((array)$row);
      }
    }

    return $view_result;
  }

}
