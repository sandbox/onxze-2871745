<?php

namespace Drupal\vjq\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
//use Drupal\Core\Url;

/**
 * Defines a form that configures devel settings.
 */
class VjqSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vjq_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vjq.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vjq.settings');

    $form['vjq_protocol'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Server URL protocol'),
      '#default_value' => $config->get('json_server.protocol'),
    );

    $form['vjq_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Server URL address'),
      '#default_value' => $config->get('json_server.url'),
    );

    $form['vjq_port'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Server URL port'),
      '#default_value' => $config->get('json_server.port'),
    );

    $form['vjq_auth'] = array(
      '#type' => 'fieldset',
      '#title' => t('Autohorization header'),
      '#tree' => TRUE,
    );

    $form['vjq_auth']['vjq_authorization'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use authentication'),
      '#default_value' => $config->get('json_server.authorization'),
      '#description' => t('Add autohorization key to header.'),
    );

    $form['vjq_auth']['vjq_authorization_prefix'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Autohorization header prefix'),
      '#description' => t('Additional information added to the start of Authorization header information eg. "Bearer ". If only one key for site, placed it here and leave "Authorization field" value empty.'),
      '#default_value' => $config->get('json_server.authorization_prefix'),
    );

    $form['vjq_auth']['vjq_authorization_field'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Authorization field'),
      '#description' => t('User field where the actual authorization key is read.'),
      '#default_value' => $config->get('json_server.authorization_field'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $auth = $form_state->getValue('vjq_auth');
    $this->config('vjq.settings')
      ->set('json_server.protocol', $form_state->getValue('vjq_protocol'))
      ->set('json_server.url', $form_state->getValue('vjq_url'))
      ->set('json_server.port', $form_state->getValue('vjq_port'))
      ->set('json_server.authorization', $auth['vjq_authorization'])
      ->set('json_server.authorization_prefix', $auth['vjq_authorization_prefix'])
      ->set('json_server.authorization_field', $auth['vjq_authorization_field'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
